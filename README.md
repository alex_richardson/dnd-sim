*TODO*

 - Make sure I've _actually_ got the rules right!
 - Import all classes and weapons
 - Implement other actions, not just melee attacks
 - -Pretty print the actor stats-
 - Add proficiencies
 
 
example interaction:

```
Choose Action:
1. New char
2. List chars
3. Attack


CHAR CREATION
Choose class
1. Fighter
2. Mage
etc...

Enter Strength
_

Enter Dexterity
_

etc...


ATTACK
Choose attacker
1. Char A
2. MyChar
3. Some other char

Choose attack
1. Melee
2. Magic

Choose defender
1. Char A
2. MyChar
3. Some other char 


RESULT
TO HIT
Roll a D20, must beat 15

DAMAGE
Roll a D8 and +3

Enter damage to update defender
Damage delt _

```

