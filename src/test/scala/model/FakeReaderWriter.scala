package model

import console.{Reader, Writer}

case class FakeReader(var input: List[String] = List.empty) extends Reader {
  override def read: String = input match {
    case x :: xs =>
      input = xs
      x
    case _ => ""
  }
}

case class FakeWriter(var output: List[String] = List.empty) extends Writer {
  override def write(content: String): Unit = output = output :+ content
}