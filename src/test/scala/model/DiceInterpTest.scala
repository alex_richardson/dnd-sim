package model

import org.specs2.mutable.Specification
import dice._

class DiceInterpTest extends Specification{
  "1d4 is a D4" in {
    DiceInterp("1d4 bludgeoning") must_== D4
  }

  "2d20 is a D20" in {
    DiceInterp("2d20 piercing") must_== D20
  }
}
