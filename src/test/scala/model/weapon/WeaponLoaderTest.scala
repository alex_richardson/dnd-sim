package model.weapon

import dice._
import org.specs2.mutable.Specification

class WeaponLoaderTest extends Specification {

  "Weapon loader can load weapons into the store" in {
    val expectedWeapons = Set(
      Weapon("Club", D4, Melee),
      Weapon("Dagger", D8, Melee)
    )
    WeaponLoader("src/test/resources/weapons/testweapons.csv", Melee) must_== expectedWeapons
  }
}
