package model

import io.circe.syntax._
import org.specs2.mutable.Specification
import io.circe.generic.auto._
import io.circe.parser._


class ActorTest extends Specification{

  "Actor is JSON de/serializable" in {
    TestActors.anyFighter().asJson must_== parse(TestActors.anyFighterJson).right.get
    decode[Actor](TestActors.anyFighterJson) must beRight(TestActors.anyFighter())
  }

}
