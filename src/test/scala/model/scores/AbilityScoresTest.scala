package model.scores

import org.specs2.mutable.Specification

class AbilityScoresTest extends Specification{

  "A score of 10 returns zero" in {
    Ability.modifier(10) must_== Zero
  }

  "A score of 20 returns five" in {
    Ability.modifier(20) must_== Plus5
  }

  "A score of 8 returns minus 1" in {
    Ability.modifier(8) must_== Minus1
  }

  "A score of 13 returns plus 1" in {
    Ability.modifier(13) must_== Plus1
  }
}
