package model

import model.Actor._
import model.scores.{Ability, AbilityScores, ArmorClass}
import model.weapon.Weapon
import Weapon._

object TestActors {

  def anyAbilityScore(armorClass: Int = 15,
                      ability: Ability = Ability(10, 10, 10, 10, 10, 10)) =
    AbilityScores(ArmorClass(armorClass), ability)

  def anyFighter(
                name : String = "some-fighter",
                weapon: Weapon = OneHandedSword,
                abilityScores: AbilityScores = anyAbilityScore()
                ) = Fighter(name, weapon, abilityScores, HitPoints(10, 10))

  val anyFighterJson = """{
                         |	"name": "some-fighter",
                         |	"weapon": {
                         |		"name": "One Handed Sword",
                         |		"damageRoll": [
                         |			"D8"
                         |		],
                         |		"attackType": "Melee"
                         |	},
                         |	"abilityScores": {
                         |		"armorClass": {
                         |			"armor": 15
                         |		},
                         |		"ability": {
                         |			"strength": 10,
                         |			"dexterity": 10,
                         |			"constitution": 10,
                         |			"intelligence": 10,
                         |			"wisdom": 10,
                         |			"charisma": 10
                         |		}
                         |	},
                         |	"hp": {
                         |		"currentHitPoints": 10,
                         |		"maxHitPoints": 10
                         |	},
                         |	"role": "Fighter"
                         |}""".stripMargin
}
