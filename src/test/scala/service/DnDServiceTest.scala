package service

import dice._
import model.scores.Zero
import model.weapon.{Melee, Weapon, WeaponStore}
import model.{Attack, TestActors, World}
import org.specs2.mutable.Specification
import org.specs2.specification
import result.{Result, Target}

class DnDServiceTest extends Specification{

  trait Scope extends specification.Scope {
    val service = DnDService(World(Set.empty), WeaponStore(Set.empty))
    val actor = TestActors.anyFighter()
  }

  "Create actor adds an actor to the world" in new Scope {
    service.createActor(actor)
    service.world.actors must contain(actor)
  }

  "Attack calculates the attack roll from one actor to the other" in new Scope {
    val attacker = TestActors.anyFighter(name = "attacker")
    val defender = TestActors.anyFighter(name = "defender")
    val move = Attack(attacker.weapon)

    service.createActor(attacker)
    service.createActor(defender)

    service.attack("attacker", "defender", move) must beSome(Result(Target(D20, 15, Zero), D8))
  }

  "Apply damage reduces hit points of actor" in new Scope {
    service.createActor(actor)
    service.applyDamage("some-fighter", 5)
    service.world.actors.toList.head.hp.currentHitPoints must_== 5
  }

  "World is unchanged if actor doesn't exist" in new Scope {
    service.applyDamage("some-fighter", 10)
    service.world must_== World(Set.empty)
  }
}
