package service

import org.specs2.mutable.Specification

import scala.io.Source

class ServerDeploymentTest extends Specification {

  "Server starts up and responds to status requests" in {
    val io = service.Server.run(List.empty)
    io.unsafeRunAsync(_ => ())
    val src = Source.fromURL("http://127.0.0.1:9001/status")
    val response = src.getLines().toList.head
    src.close()
    response must_== "dnd-sim v0.1"
  }
}
