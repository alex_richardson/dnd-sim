package route

import cats.effect.IO
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import model.weapon.WeaponStore
import model.{TestActors, World}
import org.http4s._
import org.http4s.circe._
import org.specs2.mutable.Specification
import service.DnDService
import org.http4s.implicits._

class ActorRouteTest extends Specification {

  "POST /actor -> returns OK and creates actor in world" in {
    val service  = DnDService(World(Set.empty), WeaponStore(Set.empty))
    val actorName = "actor-route-test"
    val actor = TestActors.anyFighter(name = actorName)
    val req: IO[Response[IO]] = ActorRoute(service).route.orNotFound.run(
      Request(
        method = Method.POST,
        uri = uri"""/actor"""
      ).withEntity(actor.asJson))
    req.unsafeRunSync().status must_== Status.Ok
    service.world.find(actorName) must beSome(actor)
  }

  "POST damage object at /actor/name applies damage to the actor" in {
    val actorName = "damage actor route test"
    val actor = TestActors.anyFighter(name = actorName)
    val service  = DnDService(World(Set(actor)), WeaponStore(Set.empty))
    val req: IO[Response[IO]] = ActorRoute(service).route.orNotFound.run(
      Request(
        method = Method.POST,
        uri = uri"""/actor/damageactorroutetest"""
      ).withEntity(Json.fromFields(List(("damage", Json.fromInt(4))))))
    req.unsafeRunSync().status must_== Status.Ok
    println(service.world.findById("damageactorroutetest"))
    service.world.findById("damageactorroutetest").get.hp.currentHitPoints must_== 6
  }

  "POST an invalid object to /actor returns an error" in {
    val service  = DnDService(World(Set.empty), WeaponStore(Set.empty))
    val req: IO[Response[IO]] = ActorRoute(service).route.orNotFound.run(
      Request(
        method = Method.POST,
        uri = uri"""/actor"""
      ).withEntity(Json.fromFields(List(("foo", Json.fromInt(4))))))
    req.unsafeRunSync().status must_== Status.BadRequest
  }

  "POST an invalid object to /actor/name returns an error" in {
    val service  = DnDService(World(Set.empty), WeaponStore(Set.empty))
    val req: IO[Response[IO]] = ActorRoute(service).route.orNotFound.run(
      Request(
        method = Method.POST,
        uri = uri"""/actor/actorname"""
      ).withEntity(Json.fromFields(List(("foo", Json.fromInt(4))))))
    req.unsafeRunSync().status must_== Status.BadRequest
  }
}
