package route

import cats.effect.IO
import io.circe.generic.auto._
import model.World
import model.weapon.WeaponStore
import org.http4s._
import org.http4s.circe._
import org.http4s.implicits._
import org.specs2.mutable.Specification
import service.DnDService

class WorldRouteTest extends Specification{

  "GET /world -> returns the state of the world in the service" in {
    val world = World(Set.empty)
    val service  = DnDService(world, WeaponStore(Set.empty))
    val req: IO[Response[IO]] = WorldRoute(service).route.orNotFound.run(Request(uri = uri"""/world"""))
    val response = req.unsafeRunSync()
    response.decodeJson[World].unsafeRunSync() must_== world
  }
}
