package route

import cats.effect.IO
import model.{TestActors, World}
import model.weapon.WeaponStore
import org.http4s.{Method, Request, Response, Status}
import org.specs2.mutable.Specification
import service.DnDService
import org.http4s.implicits._
import io.circe.syntax._
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe._
import result.{Result, Target}
import dice._
import model.scores.Zero

class ActionRouteTest extends Specification {

  "POST /act/attack will return attack details" in {
    val attacker = TestActors.anyFighter(name = "attacker")
    val defender = TestActors.anyFighter(name = "defender")
    val service  = DnDService(World(Set(attacker, defender)), WeaponStore(Set.empty))
    val req = ActionRoute(service).route.orNotFound.run(
      Request(
        method = Method.POST,
        uri = uri"""/act/attack"""
      ).withEntity(AttackRequest(attacker.id, defender.id, attacker.weapon).asJson))
    req.unsafeRunSync().decodeJson[Result].unsafeRunSync() must_== Result(Target(D20, 15, Zero), D8)
  }

}
