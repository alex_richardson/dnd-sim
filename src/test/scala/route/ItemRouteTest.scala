package route

import cats.effect.IO
import model.World
import model.weapon.{Weapon, WeaponStore}
import org.http4s.{Request, Response}
import org.specs2.mutable.Specification
import service.DnDService
import org.http4s.implicits._
import org.http4s._
import org.http4s.circe._

class ItemRouteTest extends Specification {

  "GET to /item/weapon returns the list from the weapon store" in {
    val world = World(Set.empty)
    val service = DnDService(world, WeaponStore(Set(Weapon.OneHandedSword)))
    val req: IO[Response[IO]] = ItemRoute(service).route.orNotFound.run(
      Request(uri = uri"""/item/weapon"""))
    val response = req.unsafeRunSync()
    response.decodeJson[List[Weapon]].unsafeRunSync() must contain(Weapon.OneHandedSword)
  }
}
