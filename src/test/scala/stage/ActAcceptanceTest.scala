package stage

import cats.data.NonEmptyList
import dice._
import model.Attack
import model.TestActors._
import model.scores._
import org.specs2.mutable.Specification
import result.{Result, Target}
import model.weapon.Weapon._
class ActAcceptanceTest extends Specification {

  "Simple attack results in 2d6 to hit and 1d8 damage" in {
    val attacker = anyFighter(name = "attacker")
    val defender = anyFighter(name = "defender")
    val result = Result(Target(D20, 15, Zero), NonEmptyList.one(D8))
    Act(attacker, defender, Attack(attacker.weapon)) must_== result
  }

  "Use defenders armor class when calculating hit target" in {
    val attacker = anyFighter(name = "attacker")
    val defender = anyFighter(name = "defender", abilityScores = anyAbilityScore(armorClass = 12))
    val result = Result(Target(NonEmptyList.one(D20), 12, Zero), D8)
    Act(attacker, defender, Attack(attacker.weapon)) must_== result
  }

  "Use attackers ability score to add modifiers" in {
    val scores = AbilityScores(ArmorClass(12), Ability(15, 12, 10, 10, 10, 10))
    val attacker = anyFighter(name = "attacker", abilityScores = scores)
    val defender = anyFighter(name = "defender")
    val result = Result(Target(D20, 15, Plus2), D8)

    Act(attacker, defender, Attack(attacker.weapon)) must_== result
  }

  "Ranged weapons use dexterity modifier" in {
    val scores = AbilityScores(ArmorClass(12), Ability(15, 8, 10, 10, 10, 10))
    val attacker = anyFighter(name = "attacker", abilityScores = scores, weapon = Wand)
    val defender = anyFighter(name = "defender")
    val result = Result(Target(D20, 15, Minus1), D4)

    Act(attacker, defender, Attack(attacker.weapon)) must_== result
  }
}
