package stage

import console.stage.CreateActorStage
import console.{Reader, Writer}
import model._
import model.scores.{Ability, AbilityScores, ArmorClass}
import model.weapon.{Weapon, WeaponStore}
import org.specs2.mutable.Specification

class CreateActorStageTest extends Specification {

  "Create actor stage can create an actor in the world" in {
    implicit val reader: Reader = FakeReader(List(
      "1",
      "test-actor",
      "0",
      "10 2 3 4 5 6 10",
      "7 9"
    ))
    implicit val writer: Writer = FakeWriter()
    val world = World(Set.empty)
    val stage = CreateActorStage(WeaponStore(Set(Weapon.OneHandedSword)))
    val expectedActor = Actor(
      name = "test-actor",
      Weapon.OneHandedSword,
      AbilityScores(ArmorClass(10), Ability(10, 2, 3, 4, 5, 6)),
      HitPoints(7, 9),
      Role.Fighter
    )

    stage(world).actors must contain(expectedActor)
  }
}
