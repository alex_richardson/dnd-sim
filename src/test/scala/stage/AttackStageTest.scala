package stage

import console.stage.AttackStage
import console.{Reader, Writer}
import model.{FakeReader, FakeWriter, World}
import org.specs2.mutable.Specification
import model.TestActors._

class AttackStageTest extends Specification{

  "Actor can melee attack another actor and record damage in the world view" in {
    implicit val reader: Reader = FakeReader(List(
      "0",
      "1",
      "4"
    ))
    implicit val writer: Writer = FakeWriter()
    val world = World(Set(anyFighter(name = "attacker"), anyFighter(name = "defender")))
    val stage = AttackStage()

    stage(world).actors.filter(_.name == "defender").head.hp.currentHitPoints must_== 6
  }
}
