package result


import cats.data.NonEmptyList
import dice._
import model.scores.Modifier

case class Target(toRoll: NonEmptyList[Dice], toBeat: Int, modifier: Modifier)

case class Result(toHit: Target, damage: NonEmptyList[Dice])

