package model

case class HitPoints(currentHitPoints: Int, maxHitPoints: Int){
  self =>
  def -(amount: Int) =
    if(currentHitPoints - amount > 0) self.copy(currentHitPoints = currentHitPoints - amount)
    else self.copy(currentHitPoints = 0)

  def +(amount: Int) =
    if(currentHitPoints + amount <= maxHitPoints) self.copy(currentHitPoints = currentHitPoints + amount)
    else self.copy(currentHitPoints = maxHitPoints)
}
