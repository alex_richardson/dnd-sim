package model

import io.circe.{Decoder, Encoder, HCursor, Json}
import model.scores.AbilityScores
import model.weapon.Weapon


case class Actor(name: String, weapon: Weapon, abilityScores: AbilityScores, hp: HitPoints, role: Role){

  val id = name.filterNot(_ == ' ').toLowerCase

  private val LEFT_COLUMN_MAX = 60
  private val topRow = {
    val classString = s"Class: ${role}"
    val nameString = s"Name: $name"
    val hpString = s"HitPoints: ${hp.currentHitPoints}/${hp.maxHitPoints}"
    classString + "\n" + nameString + List.fill(LEFT_COLUMN_MAX - nameString.length)(" ").mkString("") + hpString
  }

  private val secondRow = {
    val weaponString = s"Weapon: ${weapon.prettyPrint}"
    val ac = s"AC: ${abilityScores.armorClass.armor}"
    weaponString + List.fill(LEFT_COLUMN_MAX - weaponString.length)(" ").mkString("") + ac
  }

  val prettyPrint =
     "+------------\n" +
    topRow + "\n" +
    secondRow + "\n" +
    abilityScores.ability.prettyPrint + "\n" +
       "+------------"

}
sealed trait Role
object Role {
  final case object Fighter extends Role
  final case object Mage extends Role
  implicit val diceDecoder: Decoder[Role] = (c: HCursor) => c.as[String].map(Role(_))
  implicit val diceEncoder: Encoder[Role] = (a: Role) => Json.fromString(a.toString)
  def apply(r: String) = r match {
    case "Fighter" => Fighter
    case "Mage" => Mage
  }
}

object Actor {
  def Fighter(name: String, weapon: Weapon, abilityScores: AbilityScores, hp: HitPoints) =
    Actor(name, weapon, abilityScores, hp, Role.Fighter)
  def Mage(name: String, weapon: Weapon, abilityScores: AbilityScores, hp: HitPoints) =
    Actor(name, weapon, abilityScores, hp, Role.Mage)
}
