package model.scores

case class AbilityScores(armorClass: ArmorClass, ability: Ability)

case class ArmorClass(armor: Int)

case class Ability(strength: Int,
                   dexterity: Int,
                   constitution: Int,
                   intelligence: Int,
                   wisdom: Int,
                   charisma: Int) {
  val prettyPrint =
    s""" _______________________________________________________________________________________________
      ||   Strength    |   Dexterity   | Constitution  | Intelligence  |    Wisdom     |    Charisma   |
      ||${printStats(strength)}|${printStats(dexterity)}|${printStats(constitution)}|${printStats(intelligence)}|${printStats(wisdom)}|${printStats(charisma)}|
      ||_______________|_______________|_______________|_______________|_______________|_______________|
    """.stripMargin

  private def printStats(x: Int) = {
    if( x < 10 ){
      "       " + x + "       "
    } else {
      "      " + x + "       "
    }
  }
}

object Ability {
  private val modMap = Map(
    0 -> Minus5,
    1 -> Minus4,
    2 -> Minus3,
    3 -> Minus2,
    4 -> Minus1,
    5 -> Zero,
    6 -> Plus1,
    7 -> Plus2,
    8 -> Plus3,
    9 -> Plus4,
    10 -> Plus5,
  )
  def modifier(score: Int): Modifier = score match {
    case sc if sc <= 0 => Minus5
    case sc if sc >= 20 => Plus5
    case sc => modMap(sc / 2)
  }
}

sealed trait Modifier {
  val print: String
}
final case object Minus5 extends Modifier { override val print: String = "-5" }
final case object Minus4 extends Modifier { override val print: String = "-4" }
final case object Minus3 extends Modifier { override val print: String = "-3" }
final case object Minus2 extends Modifier { override val print: String = "-2" }
final case object Minus1 extends Modifier { override val print: String = "-1" }
final case object Zero extends Modifier { override val print: String = "0" }
final case object Plus1 extends Modifier { override val print: String = "+1" }
final case object Plus2 extends Modifier { override val print: String = "+2" }
final case object Plus3 extends Modifier { override val print: String = "+3" }
final case object Plus4 extends Modifier { override val print: String = "+4" }
final case object Plus5 extends Modifier { override val print: String = "+5" }

