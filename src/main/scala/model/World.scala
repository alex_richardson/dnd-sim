package model

case class World(actors: Set[Actor]) {
  def update(actor: Actor) = {
    World(actors.filterNot(_.name == actor.name) + actor)
  }

  def find(name: String): Option[Actor] = actors.find(_.name == name)

  def findById(id: String): Option[Actor] = actors.find(_.id == id)

  val indexedActors = actors.zipWithIndex.map{case (k, v) => v -> k}.toMap
}
