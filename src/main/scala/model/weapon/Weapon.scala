package model.weapon

import cats.data.NonEmptyList
import dice._
import io.circe.{Decoder, Encoder, HCursor, Json}


case class Weapon(name: String, damageRoll: NonEmptyList[Dice], attackType: AttackType){
  val prettyPrint =
    s"""$name - ${if(damageRoll.length == 1)  damageRoll.head else damageRoll} - $attackType"""
}

sealed trait AttackType
case object Melee extends AttackType
case object Ranged extends AttackType
object AttackType{
  implicit val decoder: Decoder[AttackType] = (c: HCursor) => c.downField("attack").as[String].map(AttackType(_))
  implicit val encoder: Encoder[AttackType] = (a: AttackType) => Json.obj(("attack", Json.fromString(a.toString)))
  def apply(a: String) = a match {
    case "Melee" => Melee
    case "Ranged" => Ranged
  }
}


object Weapon {
  val OneHandedSword = Weapon("One Handed Sword", D8, Melee)
  val BastardSword = Weapon("Bastard Sword", D8, Melee)
  val Wand = Weapon("Wand", D4, Ranged)

  implicit val diceDecoder: Decoder[Weapon] = (c: HCursor) => {
    for{
      name <- c.downField("name").as[String]
      damageRoll <- c.downField("damageRoll").as[List[String]]
      attackType <- c.downField("attackType").as[String]
    } yield Weapon(name, NonEmptyList(Dice(damageRoll.head), damageRoll.tail.map(Dice(_))), AttackType(attackType))
  }
  implicit val diceEncoder: Encoder[Weapon] = (a: Weapon) => Json.obj(
    ("name", Json.fromString(a.name)),
    ("damageRoll", Json.fromValues(a.damageRoll.map(x => Json.fromString(x.toString)).toList)),
    ("attackType", Json.fromString(a.attackType.toString))
  )
}