package model.weapon

import dice.DiceInterp

import scala.io.Source

case class WeaponStore(weapons: Set[Weapon]) {
  val printWeaponList: String = weapons.map(_.name).mkString("\n")
  val indexedWeaponList: List[(Int, Weapon)] = weapons.toList.zipWithIndex.map { case (k, v) => v -> k }
  val indexedWeaponMap: Map[Int, Weapon] = indexedWeaponList.toMap
}

object WeaponLoader {
  private def interpWeapon(attackType: AttackType)(line: String): Weapon = {
    val bits = line.split("\t")
    Weapon(bits.head, DiceInterp(bits(2)), attackType)
  }

  def apply(srcFile: String, attackType: AttackType): Set[Weapon] = {
    val src = Source.fromFile(srcFile)
    val returning = src.getLines().map(interpWeapon(attackType)).toSet
    src.close()
    returning
  }

  def loadDefaultWeaponList() = {
    val simpleMelee = WeaponLoader("src/main/resources/weapons/simplemelee.csv", Melee)
    val martialMelee = WeaponLoader("src/main/resources/weapons/martialmelee.csv", Melee)
    val simpleRanged = WeaponLoader("src/main/resources/weapons/simpleranged.csv", Ranged)
    val martialRanged = WeaponLoader("src/main/resources/weapons/martialranged.csv", Ranged)
    WeaponStore(
      simpleMelee ++ martialMelee ++ simpleRanged ++ martialRanged
    )
  }
}