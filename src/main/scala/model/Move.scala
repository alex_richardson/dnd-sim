package model

import model.scores.{Ability, AbilityScores}
import model.weapon.{Melee, Ranged, Weapon}

sealed trait Move

final case class Attack(weapon: Weapon) extends Move {
  def attackModifier(abilityScores: AbilityScores) =
    weapon.attackType match {
      case Melee => Ability.modifier(abilityScores.ability.strength)
      case Ranged => Ability.modifier(abilityScores.ability.dexterity)
    }
}
