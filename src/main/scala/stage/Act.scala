package stage

import dice._
import model.{Actor, Attack, Move}
import result.{Result, Target}

object Act {
  def apply(from: Actor, to: Actor, action: Move): Result = action match {
    case a@Attack(weapon) => Result(
      toHit = Target(toRoll = D20,
        toBeat = to.abilityScores.armorClass.armor,
        modifier = a.attackModifier(from.abilityScores)),
      damage = weapon.damageRoll)
  }
}
