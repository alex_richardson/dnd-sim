package service

import cats.effect._
import cats.implicits._
import model.World
import model.weapon.WeaponLoader
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import route.{ActorRoute, ItemRoute, StatusRoute, WorldRoute}

import scala.concurrent.ExecutionContext.Implicits.global

object Server extends IOApp {
  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  override implicit val timer: Timer[IO] = IO.timer(global)

  val service = DnDService(World(Set.empty), WeaponLoader.loadDefaultWeaponList())
  val routes =
    StatusRoute.status <+>
      WorldRoute(service).route <+>
      ActorRoute(service).route <+>
      ItemRoute(service).route

  override def run(args: List[String]): IO[ExitCode] = {
    val httpApp = routes.orNotFound
    BlazeServerBuilder[IO]
      .bindHttp(9001, "localhost")
      .withHttpApp(httpApp)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  }
}
