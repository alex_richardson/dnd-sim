package service

import model.weapon.WeaponStore
import model.{Actor, Move, World}
import result.Result
import stage.Act

//Either world access here should be wrapped in an actor
//Or the state is pushed to redis soon, bad race otherwise
case class DnDService(var world: World, weaponStore: WeaponStore) {

  def createActor(actor: Actor): Unit =
    world = world.update(actor)

  def attack(from: String, to: String, move: Move): Option[Result] =
    for{
      attacker <- world.find(from)
      defender <- world.find(to)
    } yield Act(attacker, defender, move)


  def applyDamage(id: String, amount: Int): Option[Unit] =
    for {
      actor <- world.findById(id)
    } yield world = world.update(actor.copy(hp = actor.hp - amount))
}
