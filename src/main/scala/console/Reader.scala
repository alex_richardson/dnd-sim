package console

import scala.io.StdIn

trait Reader {
  def read: String
}

trait Writer {
  def write(content: String): Unit
}

object SysInReader extends Reader {
  override def read: String = StdIn.readLine()
}

object SysOutWriter extends Writer {
  override def write(content: String): Unit = System.out.println(content)
}