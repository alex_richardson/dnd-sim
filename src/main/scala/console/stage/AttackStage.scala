package console.stage
import console.Utils._
import console.{Reader, Writer}
import model.scores.Zero
import model.{Actor, Attack, World}
import result.Target
import stage.Act

import scala.util.{Failure, Success, Try}

case class AttackStage(implicit reader: Reader, writer: Writer) extends Stage {
  self =>
  override def apply(world: World): World = {
    val attacker = chooseActor(world, "attacker")
    val defender = chooseActor(world, "defender")
    "Only supporting melee attacks for now..." ?
    val result = Act(attacker, defender, Attack(attacker.weapon))
    def rollMessage(toHit: Target) = {
      val modifier = if(toHit.modifier != Zero) s" with a ${toHit.modifier.print} modifier" else ""
      s"Roll ${toHit.toRoll.toList.mkString(", ")}$modifier, must beat ${toHit.toBeat}"
    }
    val resultText =
      s"""Must perform the following:
         |${rollMessage(result.toHit)}
         |Roll ${result.damage.toList.mkString(", ")} for damage
       """.stripMargin
    resultText ?

    applyDamage(defender, world)
  }

  def chooseActor(world: World, actorType: String): Actor = {
    s"""Choose $actorType:""" ?

    val options = world.indexedActors.mapValues(_.name).mkString("\n")
    writer.write(options)
    Try(reader.read.toInt) match {
      case Failure(_) => failedRead(chooseActor(world, actorType))
      case Success(value) => value match {
        case x if world.indexedActors.contains(x) => world.indexedActors(x)
        case _ => failedRead(chooseActor(world, actorType))
      }
    }
  }

  def applyDamage(defender: Actor, world: World): World = {
    "How much damage was caused?" ?

    Try(reader.read.toInt) match {
      case Failure(_) => failedRead(applyDamage(defender, world))
      case Success(value) => world.update(defender.copy(hp = defender.hp - value))
    }
  }
}
