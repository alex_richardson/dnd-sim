package console.stage

import console.{Reader, Writer}
import model.World
import console.Utils._
import scala.util.Try

trait Stage {
  def apply(world: World): World
  protected def failedRead[T](retry: => T)(implicit writer: Writer): T = {
    "Didn't understand that, try again..." ?

    retry
  }
  protected def readNumbers(implicit reader: Reader) = Try(reader.read.split(" ").toList.map(_.toInt))
}
