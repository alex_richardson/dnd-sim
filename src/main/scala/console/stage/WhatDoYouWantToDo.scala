package console.stage

import console.{Reader, Writer}
import model.World
import console.Utils._

case class WhatDoYouWantToDo(createActorStage: CreateActorStage, attackStage: AttackStage)(implicit reader: Reader, writer: Writer) {
  self =>
  val whatToDo =
    """
      |What would you like to do:
      |1. Print state of the world
      |2. Create an actor
      |3. Attack!
      |0. Exit
    """.stripMargin

  def apply(world: World): Unit = {
     whatToDo ?

    reader.read match {
      case "1" =>
        printWorld(world)
        self(world)
      case "2" =>
        self(createActorStage(world))
      case "3" =>
        self(attackStage(world))
      case "0" =>
        System.exit(0)
    }
  }

  def printWorld(world: World): Unit = {
    "Have the following actors:" ?

    world.actors.foreach(a => writer.write(a.prettyPrint))
  }
}
