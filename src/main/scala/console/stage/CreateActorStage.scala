package console.stage
import console.{Reader, Writer}
import model.scores.{Ability, AbilityScores, ArmorClass}
import model._
import Actor._

import scala.util.{Failure, Success, Try}
import console.Utils._
import model.weapon.{Weapon, WeaponStore}

case class CreateActorStage(store: WeaponStore)(implicit reader: Reader, writer: Writer) extends Stage {
  self =>
  val chooseClass =
    """
      |Choose class:
      |1. Fighter
      |2. Mage
    """.stripMargin
  val range = 1 to 2

  override def apply(world: World): World = {
     chooseClass ?

    Try(reader.read.toInt) match {
      case Failure(_) => failedRead(self(world))
      case Success(value) => value match {
        case 1 => world.update(createFighter)
        case 2 => world.update(createMage)
        case _ => failedRead(self(world))
      }
    }
  }

  def createFighter: Actor =
    Fighter(
      name = askName(),
      weapon = askWeapon(),
      abilityScores = askAbilityScores(),
      hp = askHP()
    )

  def createMage: Actor =
    Mage(
      name = askName(),
      weapon = askWeapon(),
      abilityScores = askAbilityScores(),
      hp = askHP()
    )

  def askWeapon(): Weapon = {
    "Enter weapon" ?

    writer.write(store.indexedWeaponList.map{case (id, weapon) => s"$id: ${weapon.name}"}.mkString("\n"))
    val weapon = readNumbers match {
      case Success(x :: _) => x
    }
    store.indexedWeaponMap.getOrElse(weapon, failedRead(askWeapon()))
  }

  def askName(): String = {
    "Enter a name" ?

    reader.read
  }

  def askAbilityScores(): AbilityScores = {
    "Enter: strength dexterity constitution intelligence wisdom charisma AC" ?

    readNumbers match {
      case Success(scores) if scores.length == 7 => createAbilityScores(scores)
      case _ => failedRead(askAbilityScores())
    }
  }

  def askHP(): HitPoints = {
    "Enter current hit points and maximum hit points" ?

    readNumbers match {
      case Success(curr :: max :: Nil) => HitPoints(curr, max)
      case _ => failedRead(askHP())
    }
  }

  def createAbilityScores(scores: List[Int]): AbilityScores =
    AbilityScores(ArmorClass(scores(6)), Ability(scores(0), scores(1), scores(2), scores(3), scores(4), scores(5)))

}
