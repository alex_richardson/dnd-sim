package console

object StdInConsole extends App{

  implicit val reader = SysInReader
  implicit val writer = SysOutWriter

  Console.run
}
