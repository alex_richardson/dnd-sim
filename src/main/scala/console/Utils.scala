package console

object Utils {

  implicit class Magic(msg: String)(implicit writer: Writer){
    def ? = writer.write(msg)
  }
}
