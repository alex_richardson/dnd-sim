package console

import console.stage.{AttackStage, CreateActorStage, WhatDoYouWantToDo}
import model.Actor.{Fighter, Mage}
import model.scores.{Ability, AbilityScores, ArmorClass}
import model.weapon.Weapon.OneHandedSword
import model.weapon.{Weapon, WeaponLoader}
import model.{HitPoints, World}

import scala.util.Random._

object Console {
  def run(implicit reader: Reader, writer: Writer): Unit = {
    val world = World(Set(
      TestActors.anyFighter(name = "Fighter McFightFace"),
      TestActors.anyMage(name = "Magey McMageFace")
    ))
    val store = WeaponLoader.loadDefaultWeaponList()
    WhatDoYouWantToDo(CreateActorStage(store), AttackStage())(reader, writer)(world)
  }
}


object TestActors {

  def rand() = nextInt(10) + 5

  def anyAbilityScore(armorClass: Int = rand(),
                      ability: Ability = Ability(rand(), rand(), rand(), rand(), rand(), rand())) =
    AbilityScores(ArmorClass(armorClass), ability)

  def anyFighter(
                  name : String = "some-fighter",
                  weapon: Weapon = OneHandedSword,
                  abilityScores: AbilityScores = anyAbilityScore()
                ) = Fighter(name, weapon, abilityScores, HitPoints(12, 12))
  def anyMage(
                  name : String = "some-fighter",
                  weapon: Weapon = OneHandedSword,
                  abilityScores: AbilityScores = anyAbilityScore()
                ) = Mage(name, weapon, abilityScores, HitPoints(12, 12))
}
