package route

import cats.effect.IO
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.dsl.io._
import service.DnDService


case class WorldRoute(service: DnDService) {
  val route: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "world"  =>
      Ok(service.world.asJson)
  }
}
