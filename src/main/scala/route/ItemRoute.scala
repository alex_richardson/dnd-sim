package route

import cats.effect._
import io.circe.syntax._
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.dsl.io._
import service.DnDService

case class ItemRoute(service: DnDService) {
  val route: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "item" / "weapon"  =>
      Ok(service.weaponStore.weapons.toList.asJson)
  }
}
