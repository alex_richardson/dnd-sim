package route

import cats.effect._
import io.circe.generic.auto._
import model.Actor
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.dsl.io._
import org.http4s.{EntityDecoder, HttpRoutes, Request}
import service.DnDService

case class ActorRoute(service: DnDService) {

  val route: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case req@POST -> Root / "actor" =>
      decodeAndApply[Actor](req, service.createActor)
    case req@POST -> Root / "actor" / actorId =>
      decodeAndApply[Damage](req, d => service.applyDamage(actorId, d.damage))
  }

  private def decodeAndApply[T](request: Request[IO], f: T => Unit)(implicit e: EntityDecoder[IO, T]) = {
    request.attemptAs[T].map {
      a => f(a)
    }.fold(_ => BadRequest(), _ => Ok()).unsafeRunSync() //Hmm, smells a bit this
  }
}
