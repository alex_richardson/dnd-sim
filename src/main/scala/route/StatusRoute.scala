package route

import cats.effect.IO
import org.http4s._
import org.http4s.dsl.io._

object StatusRoute {
  val status = HttpRoutes.of[IO] {
    case GET -> Root / "status"  =>
      Ok(s"dnd-sim v0.1")
  }
}
