package route

import cats.effect._
import io.circe.generic.auto._
import io.circe.syntax._
import model.Attack
import model.weapon.Weapon
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.dsl.io._
import service.DnDService

case class ActionRoute(service: DnDService) {

  val route: HttpRoutes[IO] = HttpRoutes.of[IO]{
    case req@POST -> Root / "act" / "attack" =>
      for {
        attackRequest <- req.as[AttackRequest]
        attackResult = service.attack(attackRequest.initiator, attackRequest.receiver, Attack(attackRequest.weapon))
        resp <- Ok(attackResult.asJson)
      } yield resp
  }
}

case class AttackRequest(initiator: String, receiver: String, weapon: Weapon)