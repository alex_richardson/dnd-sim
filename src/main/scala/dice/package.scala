import cats.data.NonEmptyList
import io.circe._

package object dice {
  sealed trait Dice
  object Dice {
    implicit val diceDecoder: Decoder[Dice] = (c: HCursor) => c.downField("dice").as[String].map(Dice(_))
    implicit val diceEncoder: Encoder[Dice] = (a: Dice) => Json.obj(("dice", Json.fromString(a.toString)))
    def apply(d: String) = d match {
      case "D4" => D4
      case "D6" => D6
      case "D8" => D8
      case "D10" => D10
      case "D12" => D12
      case "D20" => D20
      case "D100" => D100
    }
  }
  final case object D4 extends Dice
  final case object D6 extends Dice
  final case object D8 extends Dice
  final case object D10 extends Dice
  final case object D12 extends Dice
  final case object D20 extends Dice
  final case object D100 extends Dice

  implicit def itsAList(dice: Dice): NonEmptyList[Dice] = NonEmptyList.one(dice)
}
