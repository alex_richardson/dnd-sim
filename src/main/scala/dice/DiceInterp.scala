package dice

object DiceInterp {
  //This regex shit is _very_ ugly and hacky...
  private val regex = """^\dd([0-9]{1,2})""".r
  def apply(dice: String): Dice = {
    regex.findFirstIn(dice).map(_.split("d")(1)).map(d => Dice(s"D$d")).get //Need to error handle here
  }
}
